//
//  JSLocationManager.h
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 02/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol JSLocationManagerDelegate <NSObject>
-(void)locationManagerDidUpdateLocation:(CLLocation*) newLocation;
-(void)locationManagerFailedUpdatingLocation:(NSString*) errorMessage;
@end


@interface JSLocationManager : NSObject
@property (nonatomic, weak) id<JSLocationManagerDelegate> delegate;
+ (JSLocationManager *)sharedInstance;
-(void)getCurrentLocation;


@end


