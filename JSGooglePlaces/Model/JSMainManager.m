//
//  JSMainManager.m
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 03/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import "JSMainManager.h"
#import "AppHelper.h"
#import "JSConnectionManager.h"
#import "JSLocationManager.h"
#import "JSAlertController.h"
#import "JSDataManager.h"

@interface JSMainManager () <
JSLocationManagerDelegate,
JSConnectionManagerDelegate,
JSDataManagerDelegate>

@end

@implementation JSMainManager {
    CLLocation* lastLocation;
    BOOL isDuringUpdating;
    
}

- (id) init{
    self = [super init];
    
    if(self) {
        _allPlaces = [[NSMutableArray alloc] init];
        isDuringUpdating = NO;
        return self;
    }
    else {
        DLog(@"Could not initialize JSMainManager");
        return nil;
    }
}

+ (JSMainManager *)sharedInstance {
    static JSMainManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        DLog(@"JSMainManager started");
        sharedInstance = [[JSMainManager alloc] init];
    });
    return sharedInstance;
}

-(void)updatePlaces {
    if (!isDuringUpdating) {
        isDuringUpdating = YES;
        [[JSDataManager sharedInstance] removeAllLocations];
        [_allPlaces removeAllObjects];
        [JSConnectionManager sharedInstance].delegate = self;
        [JSLocationManager sharedInstance].delegate = self;
        
        [_delegate mainManagerDidStartUpdatingPlaces];
        [[JSLocationManager sharedInstance] getCurrentLocation];
    }
}


//-------------------------------------
#pragma mark - Delegate methods
//-------------------------------------
#pragma mark connectionManager
-(void)connectionManagerFailedReceivingData:(NSError *)error {
    NSString* errorMessage = [NSString stringWithFormat:@"Error receiving data: %@", error];
    DLog(@"%@",errorMessage);
    isDuringUpdating = NO;
    [_delegate mainManagerDidFailUpdatingPlaces:errorMessage];
}

-(void)connectionManagerDidReceiveData:(NSDictionary *)newData {
    DLog(@"%@",newData);
    //[JSDataManager sharedInstance].delegate = self;
    NSArray* receivedPlaces = [newData objectForKey:@"results"];
    for (NSDictionary* currentPlace in receivedPlaces) {
        [[JSDataManager sharedInstance] saveLocationwithDict:currentPlace];
    }
    
    //download next part of data if necessary
    if ([newData objectForKey:@"next_page_token"]) {
        NSString* nextPageToken = [newData objectForKey:@"next_page_token"];
        DLog("Receiving next page");
        [[JSConnectionManager sharedInstance] sendPlacesRequestWithLocation:lastLocation nextPageToken:nextPageToken];
    }
    
    _allPlaces = [NSMutableArray arrayWithArray:[[JSDataManager sharedInstance] getAllLocations]];
    isDuringUpdating = NO;
    [_delegate mainManagerDidFinishUpdatingPlaces];
}

#pragma mark LocationManager
-(void)locationManagerFailedUpdatingLocation:(NSString *)error {
    NSString* errorMessage = [NSString stringWithFormat:@"Error receiving location: %@", error];
    DLog(@"%@",errorMessage);
    isDuringUpdating = NO;
    [_delegate mainManagerDidFailUpdatingPlaces:errorMessage];
}

-(void)locationManagerDidUpdateLocation:(CLLocation *)newLocation {
    lastLocation = newLocation;
    
    //fire notification when app in the background to inform about need of finding new places
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
        UILocalNotification* notification = [[UILocalNotification alloc]init];
        [notification setAlertBody:@"Check new places in the app!"];
        [notification setFireDate:[NSDate date]];
        [notification setTimeZone:[NSTimeZone  defaultTimeZone]];
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
    else {
        //current location received -> send request to google API
        [[JSConnectionManager sharedInstance] sendPlacesRequestWithLocation:lastLocation nextPageToken:nil];
    }

}

#pragma mark DataManager
-(void)allLocationsUpdated:(NSArray *)newLocations {
    DLog(@"allLocationsUpdated: %@", newLocations);
}

@end
