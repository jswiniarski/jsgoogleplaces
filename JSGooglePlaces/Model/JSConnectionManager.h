//
//  JSConnectionManager.h
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 02/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@protocol JSConnectionManagerDelegate <NSObject>
-(void)connectionManagerDidReceiveData:(NSDictionary*) newData;
-(void)connectionManagerFailedReceivingData:(NSString*) errorMessage;
@end

@interface JSConnectionManager : NSObject
@property (nonatomic, weak) id<JSConnectionManagerDelegate> delegate;

+ (JSConnectionManager *)sharedInstance;
-(void)sendPlacesRequestWithLocation:(CLLocation*) location nextPageToken:(NSString*)nextPageToken;

@end

