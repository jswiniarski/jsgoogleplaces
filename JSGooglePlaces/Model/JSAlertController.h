
//
//  JSAlertController.h
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 03/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface JSAlertController : UIAlertController

+(void)showSettingsOKWarningDialogwithCustomTitle:(NSString *)customTitle customMessage:(NSString *)customMessage;
+(void)showOKWarningDialogwithCustomTitle:(NSString *)customTitle customMessage:(NSString *)customMessage;

@end
