//
//  JSAlertController.m
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 03/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import "JSAlertController.h"

@implementation JSAlertController

-(id)init {
    self = [super init];
    
    if(self) {
    }
    
    return self;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
    //NSLog(@"Dealloacated UIAlertController: %@", self.description);
}

-(void)hideAlertController {
    NSLog(@"Hiding UIAlertController: %@",self.description);
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
    
    [[[UIApplication sharedApplication] keyWindow].rootViewController dismissViewControllerAnimated:NO completion:nil];
}


#pragma mark - CustomWarnings
+(void)showSettingsOKWarningDialogwithCustomTitle:(NSString *)customTitle customMessage:(NSString *)customMessage {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideAlertController) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    NSString* title = [NSString stringWithFormat:@"Warning"];
    NSString* message = @"";
    
    if (customTitle) {
        title = customTitle;
    }
    if (customMessage) {
        message = customMessage;
    }
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:title
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *settingsAction = [UIAlertAction
                                     actionWithTitle:@"Settings"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action) {
                                         NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                         [[UIApplication sharedApplication] openURL:url];
                                     }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action){
                                   
                               }];
    
    [alert addAction:settingsAction];
    [alert addAction:okAction];
    
    [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:nil];
    
    
}

+(void)showOKWarningDialogwithCustomTitle:(NSString *)customTitle customMessage:(NSString *)customMessage {
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideAlertController) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    NSString* title = [NSString stringWithFormat:@"Warning"];
    NSString* message = @"";
    
    if (customTitle) {
        title = customTitle;
    }
    if (customMessage) {
        message = customMessage;
    }
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:title
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action){
                                   
                               }];
    
    [alert addAction:okAction];
    
    [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:nil];
    
    
}


@end
