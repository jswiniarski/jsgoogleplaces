//
//  JSLocation.m
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 03/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import "JSLocation.h"

@implementation JSLocation

@dynamic name;
@dynamic latitude;
@dynamic longitude;

@end
