//
//  JSLocation.h
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 03/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface JSLocation : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic) float latitude;
@property (nonatomic) float longitude;

@end
