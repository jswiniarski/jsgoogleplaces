//
//  JSLocationManager.m
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 02/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import "JSLocationManager.h"
#import "AppHelper.h"
#import "JSAlertController.h"
#import "AppDelegate.h"

@interface JSLocationManager () <CLLocationManagerDelegate>

@end

@implementation JSLocationManager {
    CLLocationManager *locationManager;
}

//--------------------
#pragma mark - Init and configuration
//--------------------
- (id) init{
    self = [super init];
    
    if(self) {
        if (locationManager == nil) {
            locationManager = [[CLLocationManager alloc] init];
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
            locationManager.distanceFilter = 5000; //next didUpdateLocations will be called after moving 5000m
            locationManager.delegate = self;
        }
        return self;
    }
    else {
        DLog(@"Could not initialize JSLocationManager");
        return nil;
    }
}


- (BOOL)checkAuthorizationStatusForCoreLocation {
    DLog(@"Checking authorization status");
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status != kCLAuthorizationStatusAuthorizedAlways &&
        status != kCLAuthorizationStatusAuthorizedWhenInUse) {
        
        DLog(@"Showing when in use authorization dialog");
        [locationManager requestAlwaysAuthorization];
        //        NSString* warnDialogTitle = [NSString stringWithFormat:@"CoreLocation", nil];
//        NSString* warnDialogMessage = [NSString stringWithFormat:NSLocalizedString(@"Allow \"whenInUse\" location access for proper location services", nil)];
//        [JSAlertController showSettingsOKWarningDialogwithCustomTitle:warnDialogTitle customMessage:warnDialogMessage];
        [_delegate locationManagerFailedUpdatingLocation:@"No authorisation for location services"];
        return NO;
    }
    else {
        return YES;
    }
}


//--------------------
#pragma mark - Public methods
//--------------------
+ (JSLocationManager *)sharedInstance {
    static JSLocationManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[JSLocationManager alloc] init];
    });
    return sharedInstance;
}

-(void)getCurrentLocation {
    if ([self checkAuthorizationStatusForCoreLocation]) {
        [locationManager startUpdatingLocation];
    }
}


//--------------------
#pragma mark - Location Manager Delegate
//--------------------
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusDenied) {
        DLog(@"AuthorisationStatusDenied");
        //TODO: can add alertController to inform the user
    }
    else if (status == kCLAuthorizationStatusAuthorizedAlways ||
             status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        DLog(@"Authorisation granted! Starting locating");
        [locationManager startUpdatingLocation];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *newLocation = [locations lastObject];
    NSLog(@"DidUpdateLocations: %@", locations);
    [_delegate locationManagerDidUpdateLocation:newLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    DLog(@"Cannot find the location. %@", error.localizedDescription);
    [_delegate locationManagerFailedUpdatingLocation:error.localizedDescription];
}

@end