//
//  JSDataManager.h
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 03/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import <Foundation/Foundation.h>
@class JSLocation;

@protocol JSDataManagerDelegate <NSObject>
//-(void)allLocationsUpdated:(NSArray*) newLocations;
@end


@interface JSDataManager : NSObject
+ (JSDataManager *)sharedInstance;
//@property (nonatomic, weak) id<JSDataManagerDelegate> delegate;

-(JSLocation*)saveLocationwithDict:(NSDictionary*) locationInfo;
-(JSLocation*)saveLocationwithName:(NSString*) name latitude:(float) latitude longitude:(float) longitude;
-(void)removeAllLocations;
-(NSArray*) getAllLocations;

@end