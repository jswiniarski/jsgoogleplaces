//
//  JSMainManager.h
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 03/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import <Foundation/Foundation.h>
@class JSLocation;

@protocol JSMainManagerDelegate <NSObject>
-(void)mainManagerDidStartUpdatingPlaces;
-(void)mainManagerDidFailUpdatingPlaces:(NSString*)errorMessage;
-(void)mainManagerDidReceiveNewPlace:(JSLocation*)newLocation;
-(void)mainManagerDidFinishUpdatingPlaces;

@end
@interface JSMainManager : NSObject
@property (nonatomic, weak) id<JSMainManagerDelegate> delegate;
+ (JSMainManager *)sharedInstance;
-(void)updatePlaces;

@property (nonatomic,readonly) NSMutableArray* allPlaces;

@end
