//
//  JSConnectionManager.m
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 02/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import "JSConnectionManager.h"
#import "AppHelper.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "Reachability.h"

@implementation JSConnectionManager {
}

//--------------------
#pragma mark - Init and configuration
//--------------------
- (id) init{
    self = [super init];
    
    if(self) {
        return self;
    }
    else {
        DLog(@"Could not initialize JSConnectionManager");
        return nil;
    }
}

+ (JSConnectionManager *)sharedInstance {
    static JSConnectionManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        DLog(@"JSConnectionManager started");
        sharedInstance = [[JSConnectionManager alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Helper Methods

-(BOOL)networkAvailable {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        DLog(@"There IS NO internet connection");
        return NO;
    } else {
        DLog(@"There IS internet connection");
        return YES;
    }
}
//--------------------
#pragma mark -
//--------------------
-(void)sendPlacesRequestWithLocation:(CLLocation*) location nextPageToken:(NSString*)nextPageToken {
    if (![self networkAvailable]) {
        DLog(@"Can't send placesRequest");
        [_delegate connectionManagerFailedReceivingData:@"No internet connection"];
        return;
    }
    
    NSString* coordinatesString = [[NSString alloc] initWithFormat:@"%f,%f", location.coordinate.latitude, location.coordinate.longitude];
    
    NSMutableDictionary* paramsDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       coordinatesString, @"location",
                                       @"500", @"radius",
                                       @"food", @"types",
                                       GooglePlacesAPIKey, @"key",
                                       
                                       nil];
    
    if (nextPageToken) {
        [paramsDict setObject:nextPageToken forKey:@"pagetoken"];
    }
    
    static NSString * const baseURL = @"https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    [manager GET:baseURL parameters:paramsDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        DLog(@"Received:\n %@", responseObject);
        [_delegate connectionManagerDidReceiveData:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DLog(@"Error receiving data: %@", error);
        [_delegate connectionManagerFailedReceivingData:error.localizedDescription];
    }];
}

@end