//
//  JSDataManager.m
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 03/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import "JSDataManager.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "JSLocation.h"
#import "AppHelper.h"

@class CLLocation;

@implementation JSDataManager {
    AppDelegate* appDelegate;
    NSManagedObjectContext* context;
}

- (id)init {
    self = [super init];
    
    if(self){
        
        NSLog(@"DataManager initialized.\n");
    }
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = appDelegate.managedObjectContext;
    
    return self;
}

+ (JSDataManager *)sharedInstance {
    static JSDataManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[JSDataManager alloc] init];
    });
    return sharedInstance;
}


-(JSLocation*)saveLocationwithDict:(NSDictionary*) locationInfoDict {
    //parse info from dictionary
    float latitude;
    float longitude;
    NSString* name;
    
    if ([locationInfoDict objectForKey:@"geometry"]) {
        NSDictionary* geometryDict = [locationInfoDict objectForKey:@"geometry"];
        if ([geometryDict objectForKey:@"location"]) {
            NSDictionary* locationDict = [geometryDict objectForKey:@"location"];
            NSString* latitudeStr = [locationDict objectForKey:@"lat"];
            NSString* longitudeStr = [locationDict objectForKey:@"lng"];
            
            latitude = [latitudeStr floatValue];
            longitude = [longitudeStr floatValue];
        }
        else {
            return nil;
        }
    }
    else {
        return nil;
    }
    
    if ([locationInfoDict objectForKey:@"name"]) {
        name = [locationInfoDict objectForKey:@"name"];
    }
    else {
        return nil;
    }

    JSLocation* location = [self saveLocationwithName:name latitude:latitude longitude:longitude];
    return location;
}

-(JSLocation*)saveLocationwithName:(NSString*) name latitude:(float) latitude longitude:(float) longitude {
    JSLocation *location = [NSEntityDescription
                            insertNewObjectForEntityForName:@"JSLocation"
                            inManagedObjectContext:[AppDelegate sharedInstance].managedObjectContext];
    
    location.name = name;
    location.latitude = latitude;
    location.longitude = longitude;
    
    NSError *error;
    if (![[AppDelegate sharedInstance].managedObjectContext save:&error]) {
        DLog(@"Error saving location: %@", [error localizedDescription]);
        return nil;
    }
    DLog(@"Location added: %@", location);
    //NSArray* allLocations = [self getAllLocations];
    //[_delegate allLocationsUpdated:allLocations];
    return location;
}

-(NSArray*) getAllLocations {
    //returns all tags stored in Core Data
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"JSLocation"
                                              inManagedObjectContext:context];
    NSError *error;
    [fetchRequest setEntity:entity];
    //[fetchRequest setReturnsObjectsAsFaults:NO];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if ([fetchedObjects count]) {
        return fetchedObjects;
    }
    else {
        NSLog(@"No Locations found in CoreData");
        DLog(@"%@",error);
        return nil;
    }
}

-(void)removeAllLocations {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSError *error;
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"JSLocation" inManagedObjectContext:context]];
    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (![fetchedObjects count]) {
        DLog(@"Error removing locations - no objects found.");
        return;
    }
    
    for (JSLocation* currentLocation in fetchedObjects) {
        [context deleteObject:currentLocation];
    }
    if (![context save:&error]) {
        DLog(@"Error saving context: %@", error);
        return;
    }
    DLog(@"All locations removed");
}

@end