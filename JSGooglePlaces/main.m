//
//  main.m
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 02/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
