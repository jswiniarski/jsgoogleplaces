//
//  JSfirstTableViewCell.h
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 04/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSfirstTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *upperLabel;
@property (strong, nonatomic) IBOutlet UILabel *bottomLabel;
@property (strong, nonatomic) IBOutlet UIImageView *customImageView;
@property (strong, nonatomic) IBOutlet UIButton *customButton;

@end
