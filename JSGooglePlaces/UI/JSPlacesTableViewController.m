//
//  JSPlacesTableViewController.m
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 02/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import "JSPlacesTableViewController.h"
#import "JSMainManager.h"
#import "JSAlertController.h"
#import "JSLocation.h"

@interface JSPlacesTableViewController () <
UITableViewDelegate, UITableViewDataSource,
JSMainManagerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *placesTableView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIButton *placesRequestButton;

@end

@implementation JSPlacesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //_placesTableView.delegate = self;
    _placesTableView.dataSource = self;
    [_activityIndicator stopAnimating];
    [JSMainManager sharedInstance].delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated {
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-------------------------------------
#pragma mark - Table view data source
//-------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[JSMainManager sharedInstance].allPlaces count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlacesCellIdentifier" forIndexPath:indexPath];
    JSLocation* location = [[JSMainManager sharedInstance].allPlaces objectAtIndex:indexPath.row];
    cell.textLabel.text = location.name;
    
    return cell;
}

//-------------------------------------
#pragma mark - Actions
//-------------------------------------
- (IBAction)sendGooglePlacesRequestPressed:(id)sender {
    [[JSMainManager sharedInstance] updatePlaces];
}


//-------------------------------------
#pragma mark - Delegate methods
//-------------------------------------
-(void)mainManagerDidStartUpdatingPlaces {
    [_activityIndicator startAnimating];
    _placesRequestButton.enabled = NO;
    [_placesTableView reloadData];
}

-(void)mainManagerDidFailUpdatingPlaces:(NSString *)errorMessage {
    [JSAlertController showOKWarningDialogwithCustomTitle:@"Error" customMessage:errorMessage];
    [_activityIndicator stopAnimating];
    _placesRequestButton.enabled = YES;
    [_placesTableView reloadData];
}

-(void)mainManagerDidFinishUpdatingPlaces {
    [_activityIndicator stopAnimating];
    _placesRequestButton.enabled = YES;
    [_placesTableView reloadData];
}


@end
