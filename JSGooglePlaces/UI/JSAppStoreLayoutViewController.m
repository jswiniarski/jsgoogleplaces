//
//  JSAppStoreLayoutViewController.m
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 04/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import "JSAppStoreLayoutViewController.h"
#import "JSfirstTableViewCell.h"
#import "CollectionViewTableViewCell.h"
#import "ImageCollectionViewCell.h"

@interface JSAppStoreLayoutViewController () <
UITableViewDelegate, UITableViewDataSource,
UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) IBOutlet UITableView *mainTableView;

@end

@implementation JSAppStoreLayoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _mainTableView.delegate = self;
    _mainTableView.dataSource = self;
    _mainTableView.rowHeight = UITableViewAutomaticDimension;
    _mainTableView.estimatedRowHeight = 70.0;
    _mainTableView.sectionHeaderHeight = 60;
}
-(void)viewWillAppear:(BOOL)animated {
    [_mainTableView reloadData];//TODO: used only for debugging
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

//-------------------------------------
#pragma mark - Table View Configuration
//-------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
        case 1:
            return 7;
        default:
            return 0;
    }
}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//
//    CBPeripheral * peripheral = [_thermometer.visiblePeripherals objectAtIndex:indexPath.row];
//
//    cell.textLabel.text = [NSString stringWithFormat:@"%@", [peripheral name]];
//    cell.imageView.image = [UIImage imageNamed:@"GPS_device_blue.png"];
//
//    return cell;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            switch (indexPath.row) {
                case 0: {
                    JSfirstTableViewCell* cell = (JSfirstTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"firstCellIdentifier"];
                    cell.customImageView.image = [UIImage imageNamed:@"sample2"];
                    cell.upperLabel.text = [NSString stringWithFormat:@"section %ld", indexPath.section];
                    cell.bottomLabel.text = [NSString stringWithFormat:@"row %ld", indexPath.row];
                    return cell;
                    break;
                }
                default: {
                    return nil;
                }
            }
            break;
        }
        case 1:{
            switch (indexPath.row) {
                case 0: {
                    CollectionViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"collectionViewTableViewCellIdentifier" forIndexPath:indexPath];
                    
                    cell.mainCollectionView.delegate = self;
                    cell.mainCollectionView.dataSource = self;
                    cell.backgroundColor = [UIColor clearColor];
                    cell.mainCollectionView.backgroundColor = [UIColor clearColor];
                    return cell;
                    break;
                }
                    
                case 1:
                case 3: {
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"descriptionCellIdentifier" forIndexPath:indexPath];
                    //TODO: add cell setup
                    return cell;
                }
                    
                case 2:
                case 4: {
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"subtitleCellIdentifier" forIndexPath:indexPath];
                    //TODO: add cell setup
                    return cell;
                }
                    
                default: {
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"defaultCellIdentifier" forIndexPath:indexPath];
                    
                    cell.textLabel.text = [NSString stringWithFormat:@"%ld-%ld", indexPath.section, indexPath.row];
                    cell.imageView.image = [UIImage imageNamed:@"sample1"];
                    return cell;
                    break;
                }
                    
            }
            break;
        }
        default: {
            return nil;
        }
    }
    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Selected tableView item: %ld", indexPath.row);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60)];
        
        NSArray* itemsArray = [NSArray arrayWithObjects:@"First", @"Second", @"Third", nil];
        UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:itemsArray];
        segmentedControl.frame = CGRectMake(5, 15, tableView.frame.size.width-10, 30);
        segmentedControl.selectedSegmentIndex = 1;
        [view addSubview:segmentedControl];
        
        [view setBackgroundColor:[UIColor whiteColor]];
        return view;
    }
    return nil;
}

//-------------------------------------
#pragma mark - UICollectionView
//-------------------------------------
#pragma mark UICollectionView Datasource
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return 8;//TODO: add model source for dynamic number of images
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imageCollectionViewCellidentifier" forIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:@"sample2"];
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    return cell;
}

#pragma mark UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Selected collectionView item: %ld", indexPath.row);
}


#pragma mark UICollectionViewDelegateFlowLayout
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //resizes images in collection view using flowLayout
    //specifies size of images in CollectionView
    CGSize retval;
    retval.width = 120;
    retval.height = 280;
    return retval;
}

-(UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}


@end
