//
//  ImageCollectionViewCell.h
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 04/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@end
