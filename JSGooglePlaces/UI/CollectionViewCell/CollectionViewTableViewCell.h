//
//  CollectionViewTableViewCell.h
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 04/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UICollectionView *mainCollectionView;

@end
