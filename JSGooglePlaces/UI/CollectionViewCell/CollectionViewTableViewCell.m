//
//  CollectionViewTableViewCell.m
//  JSGooglePlaces
//
//  Created by Jerzy Świniarski on 04/08/16.
//  Copyright © 2016 JS. All rights reserved.
//

#import "CollectionViewTableViewCell.h"

@implementation CollectionViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
